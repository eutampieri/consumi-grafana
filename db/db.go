package db

import (
	"encoding/json"
	"fmt"
	_ "github.com/influxdata/influxdb1-client"
	influxdb2 "github.com/influxdata/influxdb1-client/v2"
	"log"
	"os"
	"time"
)

type Record struct {
	Timestamp time.Time `json:"timestamp"`
	Value     float64   `json:"value"`
}

var client influxdb2.Client

func Init() {
	dbURL := os.Getenv("INFLUXDB_URL")
	if dbURL == "" {
		log.Panicln("INFLUXDB_URL must be set")
	}
	var err error
	client, err = influxdb2.NewHTTPClient(influxdb2.HTTPConfig{
		Addr: dbURL,
	})
	if err != nil {
		log.Panicln("Error creating InfluxDB Client: ", err.Error())
	}
}

func Term() {
	if err := client.Close(); err != nil {
		log.Println("error while closing influx client:", err)
	}
}

// GetEnergyUsage Connect to an Influx Database reading the credentials from
// environment variables INFLUXDB_TOKEN, INFLUXDB_URL
// return influxdb Client or errors
func GetEnergyUsage(start time.Time, end time.Time) ([]Record, error) {
	q := influxdb2.NewQuery(fmt.Sprintf(`
			SELECT energy
			FROM %s
			WHERE device = '%s'
			AND time >= '%s' AND time <= '%s'`,
		os.Getenv("INFLUXDB_MEASUREMENT"),
		os.Getenv("DEVICE_ID"),
		start.Format(time.RFC3339), end.Format(time.RFC3339),
	), os.Getenv("INFLUXDB_DB"), "")
	response, err := client.Query(q)
	if err != nil {
		log.Println("error while querying data:", err)
		return nil, err
	}
	if response.Error() != nil {
		log.Println("error in data response:", response.Error())
		return nil, response.Error()
	}

	ts := response.Results[0].Series[0]
	result := make([]Record, 0, len(ts.Values))

	for _, v := range ts.Values {
		t, err := time.Parse(time.RFC3339Nano, v[0].(string))
		if err != nil {
			continue
		}
		value, err := v[1].(json.Number).Float64()
		if err == nil {
			result = append(result, Record{t, value})
		}
	}
	return result, nil
}
