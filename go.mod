module gitlab.com/eutampieri/consumi-grafana

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/influxdata/influxdb1-client v0.0.0-20220302092344-a9ab5670611c
	github.com/rs/cors v1.8.2
)
