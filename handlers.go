package main

import (
	"encoding/json"
	"gitlab.com/eutampieri/consumi-grafana/db"
	"log"
	"net/http"
	"time"
)

func atMidnight(t time.Time) time.Time {
	yy, mm, dd := t.Date()
	t1 := time.Date(yy, mm, dd, 0, 0, 0, 0, locale)
	return t1
}

func handleEnergyRequest(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var qR queryRequest
	err := decoder.Decode(&qR)
	if err != nil {
		log.Println("error decoding query", err)
		w.WriteHeader(400)
		return
	}
	log.Printf("printing the query request %+v", qR)

	var a []db.Record
	if qR.Targets[0].Target == "midnight" {
		a, _ = db.GetEnergyUsage(atMidnight(qR.Range.To), qR.Range.To)
	} else {
		a, _ = db.GetEnergyUsage(qR.Range.From, qR.Range.To)
	}
	usageByRate := CalcUsageByRate(ToMonotonicSeries(a))

	qRsp := QueryTableResponse{
		{
			Columns: []TableResponseColumn{
				{Text: "F1"},
				{Text: "F23"},
			},
			Type: "table",
			Rows: [][]float64{{
				usageByRate[F1],
				usageByRate[F2] +
					usageByRate[F3],
			}},
		},
	}
	json.NewEncoder(w).Encode(qRsp)
}
