package main

import (
	"gitlab.com/eutampieri/consumi-grafana/db"
	"time"
)

var locale *time.Location

type Rate int

const (
	F1 Rate = iota
	F2
	F3
)

func TimeToRate(t time.Time) Rate {
	t = t.In(locale)
	if t.Hour() == 23 || t.Hour() <= 6 ||
		t.Weekday() == time.Saturday || t.Weekday() == time.Sunday {
		return F3
	} else if t.Hour() == 7 || (t.Hour() >= 19 && t.Hour() <= 22) {
		return F2
	} else {
		return F1
	}
}

func ToMonotonicSeries(s []db.Record) []db.Record {
	result := make([]db.Record, 0, len(s))
	if len(s) == 0 {
		return result
	}
	add := s[0].Value * -1
	last := s[0].Value
	for _, record := range s {
		if record.Value < last {
			add += last
		}
		last = record.Value
		result = append(result, db.Record{
			Timestamp: record.Timestamp,
			Value:     record.Value + add,
		})
	}
	return result
}

func CalcUsageByRate(s []db.Record) []float64 {
	result := make([]float64, F3+1)
	last := s[0].Value
	current := TimeToRate(s[0].Timestamp)
	for i, record := range s {
		if r := TimeToRate(record.Timestamp); r != current || i == len(s)-1 {
			if i == len(s)-1 {
				i++
			}
			result[current] += s[i-1].Value - last
			current = r
			last = record.Value
		}
	}
	return result
}
