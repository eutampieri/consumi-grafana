package main

import (
	"context"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/eutampieri/consumi-grafana/db"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var srv *http.Server

func init() {
	db.Init()
	var err error
	locale, err = time.LoadLocation("Europe/Rome")
	if err != nil {
		log.Panicln("Missing Europe/Rome TZ")
	}
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "4444"
	}

	// cors: ["*"]
	c := cors.Default()
	/*c := cors.New(cors.Options{
		AllowedOrigins: []string{
			"https://my.example.com",
			"http://localhost:4444",
			"https://*.eugi.com"},
		AllowCredentials: true,
	})*/

	mainRouter := mux.NewRouter()
	mainRouter.HandleFunc("/energy", func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(200)
	})
	//mainRouter.HandleFunc("/energy/search", s.handle) //.
	mainRouter.HandleFunc("/energy/query", handleEnergyRequest)

	srv = &http.Server{
		Addr:    ":" + port,
		Handler: c.Handler(mainRouter),
	}
	// async start http server
	go func() {
		log.Printf("serving on 0.0.0.0:%s", port)
		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Panicln(err)
		} else {
			log.Println("http server closed")
		}
	}()

	shutdownChan := make(chan os.Signal, 1)
	signal.Notify(shutdownChan, syscall.SIGINT)
	signal.Notify(shutdownChan, syscall.SIGTERM)
	<-shutdownChan
	shutdown()
}

func shutdown() {
	log.Println("shutting down...")

	// shutdown http server first
	err := srv.Shutdown(context.Background())
	if err != nil {
		log.Printf("error while shutting down http server: %v", err)
	}

	// shutdown db connection
	db.Term()
}
